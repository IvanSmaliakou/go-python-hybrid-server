# server
# consider this version as stable
Setup: 
- install python3 for your OS
- git clone https://gitlab.com/IvanSmaliakou/go-python-hybrid-server.git
- go get -u ./...
- change the line in service/service.go from #include <Python/Python.h> to #include <Python.h>
- export PORT={PORT}

Run:
- go run .

 - 15000 QPS;
 - Task took 20 hours

Problems:
  - When run with multiple goroutines, at some point the program starts to get slower
  - To run tests, you need to change the line PyObject *folder_path = PyUnicode_FromString((const char *) "./python/logic")
  to line PyObject *folder_path = PyUnicode_FromString((const char *) "{YOUR_PATH_TO_FILE}/python/logic")
 