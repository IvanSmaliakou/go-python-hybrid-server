package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/valyala/fasthttp"
	"gitlab.com/go-python-hybrid-server/routing"
	"gitlab.com/go-python-hybrid-server/service"
	"gitlab.com/go-python-hybrid-server/utils"
)

/*
#cgo LDFLAGS: -framework Python

void initPy(){
	Py_Initialize();
}

void destrPy(){
    Py_Finalize();
}
*/
import "C"

func main() {
	s := service.NewService()
	values, err := parseJSON("resources/data.json")
	if err != nil {
		log.Fatalf("failed to parse json file: %s", err)
	}
	handler := routing.NewHandler(s, values)
	C.initPy()
	if err := fasthttp.ListenAndServe(fmt.Sprint(":", utils.GetPort()), handler.RequestHandler); err != nil {
		log.Fatalf("unexpected error in server: %s", err)
	}
	C.destrPy()
}

func parseJSON(filename string) (map[string]map[string]int64, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var m map[string]map[string]int64
	err = json.Unmarshal(data, &m)
	if err != nil {
		return nil, err
	}
	return m, nil
}
