module gitlab.com/go-python-hybrid-server

go 1.14

require (
	github.com/stretchr/testify v1.5.1
	github.com/valyala/fasthttp v1.12.0
	github.com/valyala/fastjson v1.5.1
)
