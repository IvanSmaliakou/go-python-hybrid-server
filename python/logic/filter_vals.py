import json

# filter_values filters str1, str2 so that the key with highest value will remain in resulting json object.
def filter_values(str1, str2):
    ret = dict()
    ser1 = json.loads(str1)
    ser2 = json.loads(str2)
    for key1 in ser1:
        ret[key1] = ser1[key1]
    for key2 in ser2:
        if ret.get(key2, "") is not "":
            if ret.get(key2) < ser2[key2]:
                ret[key2] = ser2[key2]
        else:
            ret[key2] = ser2[key2]

    return json.dumps(ret)
