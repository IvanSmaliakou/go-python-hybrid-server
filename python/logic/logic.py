# filter_text splits text into separate words
def filter_text(text):
    return text.split(" ")
