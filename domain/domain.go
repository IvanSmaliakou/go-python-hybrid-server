package domain

import "sync"

// Response is a struct which represents response, returned by FilterText.
type Response struct {
	Text   string `json:"text"`
	Topics string `json:"topics"`
}

// Values is a map of synchronized values
type Values struct {
	Mx     sync.RWMutex
	Values map[string]map[string]int64
}
