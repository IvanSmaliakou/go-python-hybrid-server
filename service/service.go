package service

/*
#cgo LDFLAGS: -framework Python
#include <stdio.h>
#include <Python/Python.h>


char* filter_values(char *string1, char* string2) {
    PyObject *sys = PyImport_ImportModule("sys");
    PyObject *sys_path = PyObject_GetAttrString(sys, "path");
    PyObject *folder_path = PyUnicode_FromString((const char *) "./python/logic");
    PyList_Append(sys_path, folder_path);

    PyObject *pName = PyUnicode_FromString("filter_vals");
    if (!pName) {
        PyErr_Print();
        return NULL;
    }

    PyObject *pModule = PyImport_Import(pName);
    if (!pModule) {
        PyErr_Print();
        return NULL;
    }

    PyObject *pDict = PyModule_GetDict(pModule);
    if (!pDict) {
        PyErr_Print();
        return NULL;
    }
    PyObject *pObjct = PyDict_GetItemString(pDict, (const char *) "filter_values");
    if (!pObjct) {
        PyErr_Print();
        return NULL;
    }
    PyObject *retVal = PyObject_CallFunction(pObjct, (char *) "(ss)", string1, string2);
    if (!retVal) {
        PyErr_Print();
        return NULL;
    }
    char* str = PyBytes_AS_STRING(retVal);
	return str;
}

char **split_words(char *string) {
	PyObject *sys = PyImport_ImportModule("sys");
	PyObject *sys_path = PyObject_GetAttrString(sys, "path");
	PyObject *folder_path = PyUnicode_FromString((const char *) "./python/logic");
	PyList_Append(sys_path, folder_path);


	PyObject *pName = PyUnicode_FromString("logic");
	if (!pName) {
	PyErr_Print();
	return NULL;
	}

	PyObject *pModule = PyImport_Import(pName);
	if (!pModule) {
	PyErr_Print();
	return NULL;
	}

	PyObject *pDict = PyModule_GetDict(pModule);
	if (!pDict) {
	PyErr_Print();
	return NULL;
	}
	PyObject *pObjct = PyDict_GetItemString(pDict, (const char *) "filter_text");
	if (!pObjct) {
	PyErr_Print();
	return NULL;
	}

	PyObject *retVal = PyObject_CallFunction(pObjct, (char *) "(s)", string);
	if (!retVal) {
	PyErr_Print();
	return NULL;
	}

	char **array = calloc(0, sizeof(char *));

	for (int i = 0; i < PyList_Size(retVal); i++) {
	PyObject *item = PyList_GetItem(retVal, i);
	array[i] = PyBytes_AS_STRING(item);
	}
	return array;
}
*/
import "C"

import (
	"encoding/json"
	"errors"
	"gitlab.com/go-python-hybrid-server/domain"
	"unsafe"
)

// Service is a service struct.
type Service struct {
}

// NewService creates new Service struct instance.
func NewService() *Service {
	return &Service{}
}

// FilterText filters text according to rules in data.json.
func (s *Service) FilterText(text []byte, values *domain.Values) (string, error) {
	list := C.split_words(C.CString(string(text)))
	words := s.convertToStringList(list)
	topics := make(map[string]int64)
	var allData []string
	var err error

	for _, el := range words {
		for i := 0; i < 100; i++ {
			topics, err = s.getTopics(el, values)
			if err != nil {
				return "", err
			}
		}
		data, err := json.Marshal(topics)
		if err != nil {
			return "", err
		}
		allData = append(allData, string(data))
	}
	if len(allData) != 2 {
		return "", errors.New("len of allData have to be equal to 2.")
	}
	cStr := C.filter_values(C.CString(allData[0]), C.CString(allData[1]))
	return C.GoString(cStr), nil
}

// getTopics returns map of topics from values by key, provided in word.
func (s *Service) getTopics(word string, values *domain.Values) (map[string]int64, error) {
	values.Mx.RLock()
	defer values.Mx.RUnlock()
	v, ok := values.Values[word]
	if !ok {
		return nil, errors.New("failed to read map with values")
	}
	return v, nil
}

// convertToStringList parses two-dimensional C array (char** arr) into the string list
func (s *Service) convertToStringList(list **C.char) []string {
	tmpslice := (*[1 << 30]*C.char)(unsafe.Pointer(list))[:2]
	gostrings := make([]string, 0)
	for i := range tmpslice {
		t := C.GoString(tmpslice[i])
		if t != "" {
			gostrings = append(gostrings, t)
		}
	}
	return gostrings
}
