package service

import (
	"errors"
	"gitlab.com/go-python-hybrid-server/domain"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewService(t *testing.T) {
	s := NewService()
	require.NotNil(t, s)
}

func TestService_FilterText(t *testing.T) {
	type out struct {
		result      string
		expectedErr error
	}
	type in struct {
		text   []byte
		values *domain.Values
	}
	tt := []struct {
		name string
		in   *in
		out  *out
	}{
		{
			name: "getTopics error",
			in: &in{text: []byte("test 55"), values: &domain.Values{Values: map[string]map[string]int64{"test": {"1": 0, "2": 3},
				"54": {"1": 2, "12": 5}}}},
			out: &out{result: "", expectedErr: errors.New("failed to read map with values")},
		},
		{
			name: "allData len() is 0",
			in: &in{text: []byte("test12"), values: &domain.Values{Values: map[string]map[string]int64{"test": {"1": 0, "2": 3},
				"55": {"1": 2, "12": 5}}}},
			out: &out{result: "", expectedErr: errors.New("len of allData have to be equal to 2.")},
		},
		{
			name: "success",
			in: &in{text: []byte("test 55"), values: &domain.Values{Values: map[string]map[string]int64{"test": {"1": 0, "2": 3},
				"55": {"1": 2, "12": 5}}}},
			out: &out{result: `{"1": 2, "12": 5, "2": 3}`},
		},
	}
	s := NewService()
	for _, tc := range tt {
		res, err := s.FilterText(tc.in.text, tc.in.values)
		require.Equal(t, tc.out.expectedErr, err)
		if tc.out.expectedErr == nil {
			require.Equal(t, tc.out.result, res)
		}
	}
}

func BenchmarkService_FilterText(b *testing.B) {
	s := NewService()
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		if _, err := s.FilterText([]byte("test 55"), &domain.Values{Values: map[string]map[string]int64{"test": {"1": 0, "2": 3},
			"55": {"1": 2, "12": 5}}}); err != nil {
			return
		}
	}
}
