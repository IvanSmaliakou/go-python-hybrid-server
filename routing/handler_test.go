package routing

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/go-python-hybrid-server/service"
)

func TestNewHandler(t *testing.T) {
	s := service.NewService()
	h := NewHandler(s, make(map[string]map[string]int64))
	require.NotNil(t, h)
}
