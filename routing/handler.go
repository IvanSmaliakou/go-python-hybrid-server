package routing

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"gitlab.com/go-python-hybrid-server/domain"
	"gitlab.com/go-python-hybrid-server/service"
	"log"
	"net/http"
)

// Handler is a request handler struct.
type Handler struct {
	service *service.Service
	values  *domain.Values
}

// NewHandler creates new handler.
func NewHandler(s *service.Service, values map[string]map[string]int64) *Handler {
	return &Handler{
		service: s,
		values:  &domain.Values{Values: values},
	}
}

// RequestHandler handles request.
func (h *Handler) RequestHandler(ctx *fasthttp.RequestCtx) {
	body := ctx.Request.Body()
	reqValue, err := fastjson.ParseBytes(body)
	if err != nil {
		ctx.SetStatusCode(http.StatusInternalServerError)
		_, err := ctx.WriteString(fmt.Sprintf("error while parsing request: %s", err))
		if err != nil {
			log.Fatal("error while writing response: ", err)
		}
	}

	text := reqValue.GetStringBytes("text")
	topics, err := h.service.FilterText(text, h.values)
	if err != nil {
		ctx.SetStatusCode(http.StatusInternalServerError)
		_, err := ctx.WriteString(fmt.Sprintf("error while parsing request: %s", err))
		if err != nil {
			log.Fatal("error while writing response: ", err)
		}
	}
	encTopics := &domain.Response{
		Text:   string(text),
		Topics: topics,
	}
	data, err := json.Marshal(encTopics)
	if err != nil {
		ctx.SetStatusCode(http.StatusInternalServerError)
		_, err := ctx.WriteString(fmt.Sprintf("error while parsing request: %s", err))
		if err != nil {
			log.Fatal("error while writing response: ", err)
		}
	}
	_, err = ctx.Write(data)
	if err != nil {
		log.Fatal("error while writing response: ", err)
	}
}
