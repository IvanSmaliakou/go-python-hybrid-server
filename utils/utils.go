package utils

import "os"

// GetPort returns PORT environment variable, otherwise returns "8080"
func GetPort() string {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	return port
}
