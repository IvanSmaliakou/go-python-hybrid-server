package utils

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetPort(t *testing.T) {
	tt := []struct {
		name string
		in   string
		out  string
	}{
		{
			name: "empty",
			in:   "",
			out:  "8080",
		},
		{
			name: "success",
			in:   "test",
			out:  "test",
		},
	}

	for _, tc := range tt {
		require.NoError(t, os.Setenv("PORT", tc.in))
		require.Equal(t, tc.out, GetPort())
	}
}
